// Export that fixes Typescript Cannot redeclare block-scoped variable.
export {};
const {
  userRepository,
} = require('../repositories');
/**
 * UserProvider class encapsulates User related tasks
 *
 * @class UserProvider
 */
class UserProvider {

  /**
   * updates a user
   *
   * @param {Number} userId
   * @param {Object} updateData
   * @returns {Promise<Boolean>}
   * @memberof UserProvider
   */
  async updateById(userId: Number, updateData: Object) {
    return userRepository.updateById(userId, updateData);
  }

  /**
   * Read a user by id
   * @param {Number} id
   * @returns {Promise<UserModel>}
   * @memberof UserProvider
   */
  async findById(id: Number) {
    return userRepository.findById(id);
  }

  /**
   * Read a user by email
   * @param {String} email
   * @returns {Promise<UserModel>}
   * @memberof UserProvider
   */
  async findByEmail(email: String) {
    return userRepository.findByEmail(email);
  }
  

  /**
   * create user
   * @param {Object} newData
   * @returns {Promise<UserModel>}
   * @memberof UserProvider
   */
  async create(newData: Object) {
    return userRepository.create(newData);
  }
}

module.exports = UserProvider;
