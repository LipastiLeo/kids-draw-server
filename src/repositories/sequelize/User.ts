// Export that fixes Typescript Cannot redeclare block-scoped variable.
export {};
const { UserModel } = require('../../models');

/**
 * @property {Number} count
 * @property {UserModel[]} rows
 */
const userRepository = {
  /**
   * Creates a new user
   *
   * @param {UserModel} userData
   * @returns {Promise<UserModel>}
   */
  create: (userData: typeof UserModel) => UserModel.create(userData),
  /**
   * Read by id
   *
   * @param {Number} id - database id
   * @returns {Promise<UserModel>}
   */
  findById: (id: Number) => UserModel.findByPk(id),
  /**
   * Read by email
   *
   * @param {String} email - database email
   * @returns {Promise<UserModel>}
   */
  findByEmail: (email: String) => UserModel.findOne({ where: { email: email } }),
  /**
   * Full update of a user
   *
   * @param {Number} id - database id
   * @param {Object} updateData
   * @returns {Promise<Boolean>}
   */
  updateById: async (id: Number, updateData: Object) => {
    const existingRecord = await UserModel.findByPk(id);
    if (!(existingRecord && (existingRecord.id === id))) {
      throw new Error(`User #${id} does not exist.`);
    }
    const updateResult = await UserModel.update(updateData, { where: { id } });
    return (Array.isArray(updateResult) && (updateResult[0] !== 0));
  },
};

module.exports = userRepository;
