// Export that fixes Typescript Cannot redeclare block-scoped variable.
export {};
const { SubuserModel } = require('../../models');

/**
 * @typedef {Object} SubuserListResult
 */
const subuserRepository = {
  /**
   * Subusers list
   *
   * @returns {Promise<SubuserListResult>}
   */
  list: async (options: Object) => {
    return SubuserModel.findAndCountAll(options);
  },
  /**
   * Creates a new user
   *
   * @param {SubuserModel} userData
   * @returns {Promise<SubuserModel>}
   */
  create: (userData: typeof SubuserModel) => SubuserModel.create(userData),
  /**
   * Full update of a user
   *
   * @param {Number} id - database id
   * @param {Object} updateData
   * @returns {Promise<Boolean>}
   */
  updateById: async (id: Number, updateData: Object) => {
    const existingRecord = await SubuserModel.findByPk(id);
    if (!(existingRecord && (existingRecord.id === id))) {
      throw new Error(`User #${id} does not exist.`);
    }
    const updateResult = await SubuserModel.update(updateData, { where: { id } });
    return (Array.isArray(updateResult) && (updateResult[0] !== 0));
  },
};

module.exports = subuserRepository;
