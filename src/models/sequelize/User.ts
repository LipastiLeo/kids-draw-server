// Export that fixes Typescript Cannot redeclare block-scoped variable.
export {};
const { Sequelize, DataTypes } = require("sequelize");
const sql = require('../../sql');

/**
 * @typedef {Object} UserModel
 * @property {Number} id
 * @property {String} username
 * @property {String} email
 * @property {String} password
 * @property {Boolean} active
 * @property {String} reg_date
 */

/**
 * @type {UserModel}
 */
const User = sql.define('users', {
  /**
   * id, primary key
   */
  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.BIGINT,
    field: 'id',
    validate: {
      isInt: true
    }
  },
  /**
   * username
   */
  username: {
    type: Sequelize.STRING(255),
    field: 'username',
    allowNull: false,
    validate: {
      notNull: {
        msg: 'Please give a username.'
      }
    }
  },
  /**
   * email
   */
  email: {
    unique: true,
    type: Sequelize.STRING(255),
    field: 'email',
    allowNull: false,
    validate: {
      notNull: {
        msg: 'Please enter your email.'
      }
    }
  },
  /**
   * password
   */
  password: {
    type: DataTypes.STRING(64),
    is: /^[0-9a-f]{64}$/i,
    field: 'password',
    allowNull: false,
    validate: {
      notNull: {
        msg: 'Please give a password for your account.'
      }
    }
  },
  /**
   * active - used to set false instead of delete
   */
  active: {
    type: Sequelize.BOOLEAN,
    field: 'active'
  },
  /**
   * update date
   */
  reg_date: {
    allowNull: true,
    type: Sequelize.DATE,
    field: 'reg_date',
    validate: {
      isDate: true
    }
  },
});

module.exports = User;