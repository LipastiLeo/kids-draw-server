// Export that fixes Typescript Cannot redeclare block-scoped variable.
export {};
const {
  subuserRepository,
} = require('../repositories');
/**
 * SubuserProvider class encapsulates User related tasks
 *
 * @class SubuserProvider
 */
class SubuserProvider {

  /**
   * updates a subuser
   *
   * @param {Number} userId
   * @param {Object} updateData
   * @returns {Promise<Boolean>}
   * @memberof SubuserProvider
   */
  async updateById(userId: Number, updateData: Object) {
    return subuserRepository.updateById(userId, updateData);
  }
}

module.exports = SubuserProvider;
