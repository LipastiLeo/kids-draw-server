import { Data } from '../types/jsondata';

/**
 * Controller class
 *
 * @class Controller
 * @property {object} req Express request
 * @property {object} res Express result
 * @property {any} presenter Presenter
 * @property {any} model Current model
 */

class Controller {
  req: Object;
  res: Object;
  presenter: any;
  model: any;
  /**
   * Creates an instance of Controller.
   * @param {Object} req Express request
   * @param {Object} res Express result
   * @memberof Controller
   */
  constructor(req: Object, res: Object) {
    this.req = req;
    this.res = res;
    this.presenter = this.presenter;
    this.model = this.model;
  }

  /**
   * Render data according to presenter rules
   *
   * @param {Data} data
   * @returns {object}
   * @memberof Controller
   */
  render(data: Data) {
    return this.presenter.render(data);
  }

}

module.exports = Controller;