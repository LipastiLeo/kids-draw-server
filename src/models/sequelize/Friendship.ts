// Export that fixes Typescript Cannot redeclare block-scoped variable.
export {};
const { Sequelize, DataTypes } = require("sequelize");
const sql = require('../../sql');

/**
 * @typedef {Object} FriendshipModel
 * @property {Number} id
 * @property {Number} receiverid
 * @property {Number} requesterid
 * @property {String} uniqcode
 * @property {Boolean} accepted
 * @property {Boolean} active
 * @property {String} reg_date
 */

/**
 * @type {FriendshipModel}
 */
const Friendship = sql.define('friendships', {
  /**
   * id, primary key
   */
  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.BIGINT,
    field: 'id',
    validate: {
      isInt: true
    }
  },
  /**
  * receiverid
  */
  receiverid: {
    allowNull: false,
    foreignKey: true,
    type: Sequelize.BIGINT,
    field: 'receiverid',
    validate: {
      isInt: true
    }
  },
  /**
  * requesterid
  */
  requesterid: {
    allowNull: false,
    foreignKey: true,
    type: Sequelize.BIGINT,
    field: 'requesterid',
    validate: {
      isInt: true
    }
  },
  /**
   * uniqcode
   */
  uniqcode: {
    unique: true,
    type: Sequelize.STRING(255),
    field: 'uniqcode'
  },
  /**
   * accepted - default false, receiver party has requested
   */
  accepted: {
    type: Sequelize.BOOLEAN,
    field: 'accepted'
  },
  /**
   * active - default true, used to set false instead of delete
   */
  active: {
    type: Sequelize.BOOLEAN,
    field: 'active'
  },
  /**
   * update date
   */
  reg_date: {
    allowNull: true,
    type: Sequelize.DATE,
    field: 'reg_date',
    validate: {
      isDate: true
    }
  }
});

module.exports = Friendship;