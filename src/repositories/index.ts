const userRepository = require('./sequelize/User');
const subuserRepository = require('./sequelize/Subuser');

module.exports = {
  userRepository,
  subuserRepository,
};
