const bcrypt = require('bcrypt');

/**
 * BcryptProvider
 *
 * @class BcryptProvider
 */
class BcryptProvider {
  /**
   * Bcrypt Encrypt
   * 
   * @param {String} password
   * @param {Number} saltRounds
   * @returns {String}
   */
  async bcryptEncrypt (password: String, saltRounds: Number) {
    const result = await new Promise((resolve, reject) => {
      bcrypt.hash(password, saltRounds, function(err: any, hash: any) {
        if (err) reject(err)
        resolve(hash)
      });
    }).catch((error) => {
      throw new Error(error);
    });
    return result;
  }

  /**
   * Bcrypt Compare passwords
   * 
   * @param {String} password
   * @param {String} dbpassword
   * @returns {Boolean}
   */
  async comparePasswords (password: String, dbpassword: String) {
    const result = await new Promise((resolve, reject) => {
      bcrypt.compare(password, dbpassword, function(err: any, res: boolean) {
        if (err) reject(err)
        resolve(res)
      });
    }).catch((error) => {
      throw new Error(error);
    });
    return result;
  }
}

module.exports = BcryptProvider;
