const express = require('express');
import { MiddlewareFn } from '../types/middleware';
const jwt = require('jsonwebtoken');
const router = express.Router();
const UserController = require('../controllers/UserController');

const accessTokenSecret = process.env.accessTokenSecret || 'localdevTokens';
const refreshTokenSecret = process.env.refreshTokenSecret || 'localdevTokensRefresh';

// login with email and password
router.post('/', <MiddlewareFn>async function (req, res) {

    const { email, password } = req.body.data.attributes;

    // Check password and username here then return userid
    let userid = 0;

    const controller = new UserController(req, res);
    userid = await controller.checkPassword(
        String(email),
        String(password)
    );

    if (userid !== 0) {
        // Generate an access token
        const accessToken = jwt.sign({ id: userid }, accessTokenSecret, { expiresIn: '15m' });
        // Generate refresh token. accessToken is passed to refreshtoken for validation. 12h is max for a user to be online (using admin features) in a session
        const refreshToken = jwt.sign({ id: userid }, refreshTokenSecret, { expiresIn: '12h' });
        res.cookie('refresh_token', refreshToken, {
            maxAge: 86_400_000,
            httpOnly: true,
            sameSite: 'strict'
            })
        .json({
            accessToken
        });
    } else {
        res.sendStatus(401);
    }
});

module.exports = router;