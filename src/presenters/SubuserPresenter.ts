// Export that fixes Typescript Cannot redeclare block-scoped variable.
export {};
// Yason - serializing and reading JSON API data
const yayson = require('yayson')();
const { Presenter } = yayson;

class SubuserPresenter extends Presenter {}
SubuserPresenter.prototype.type = 'subusers';

module.exports = SubuserPresenter;
