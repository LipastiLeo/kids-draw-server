// Export that fixes Typescript Cannot redeclare block-scoped variable.
export {};
import { MiddlewareFn } from '../types/middleware';
import { User } from '../types/jwt';
const jwt = require('jsonwebtoken');

const accessTokenSecret = process.env.accessTokenSecret || 'localdevTokensToken';
const refreshTokenSecret = process.env.refreshTokenSecret || 'localdevTokensRefresh';

const authMiddleware = <MiddlewareFn>function (req, res) {
  const authHeader = req.headers.authorization;
  const authCookie = req.cookies.refresh_token;

  if (authHeader) {
    // verify authtoken and check that error message is expired
    const token = authHeader.split(' ')[1];
    jwt.verify(token, accessTokenSecret, (err: Error, decoded: User) => {
      if (!!err && err.name === 'TokenExpiredError') {
        // expiry error is what we want. Now verify refreshtoken
        jwt.verify(authCookie, refreshTokenSecret, (err: Object, decoded: User) => {
          if (err) {
              // refresh token was wrong
              return res.sendStatus(403);
          }
          // Generate an access token
          const accessToken = jwt.sign({ id: decoded.id }, accessTokenSecret, { expiresIn: '10m' });
          res.json({
              accessToken
          });
        });
      } else {
        // case1: someone sends a refresh request and token is not expired (is valid), return 403
        // case2: someone sends refresh request and token is wrong, return 403
        return res.sendStatus(403);
      }
    });
  } else {
      res.sendStatus(401);
  }
};

module.exports = authMiddleware;