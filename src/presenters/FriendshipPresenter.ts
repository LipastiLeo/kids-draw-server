// Export that fixes Typescript Cannot redeclare block-scoped variable.
export {};
// Yason - serializing and reading JSON API data
const yayson = require('yayson')();
const { Presenter } = yayson;

class FriendshipPresenter extends Presenter {}
FriendshipPresenter.prototype.type = 'friendships';

module.exports = FriendshipPresenter;
