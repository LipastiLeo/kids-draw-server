// Export that fixes Typescript Cannot redeclare block-scoped variable.
export {};
const HttpStatus = require('http-status-codes');
const yayson = require('yayson');
const Controller = require('./Controller');
const UserModel = require('../models/sequelize/User');
const { UserPresenter } = require('../presenters');
const UserProvider = require('../providers/UserProvider');
const BcryptProvider = require('../providers/BcryptProvider');

const bcrypt = require('bcrypt');

const { Store } = yayson();

/**
 * @class UserController
 */
class UserController extends Controller {
  constructor(req: Object, res: Object) {
    super(req, res);
    this.presenter = UserPresenter;
    this.model = UserModel;
  }
  
  /**
   * Check Password
   *
   * @param {String} email
   * @param {String} password
   */
  async checkPassword(email: String, password: String) {

    try {
      const userProvider = new UserProvider();
      const result = await userProvider.findByEmail(email);

      if (!result) {
        throw new Error('Wrong email or password.');
      }

      const saltRounds = 10;

      const bcryptProvider = new BcryptProvider();
    
      const comparePassword = await bcryptProvider.comparePasswords(password, result.dataValues.password);

      // check password
      if (comparePassword) {
        return result.dataValues.id;
      } else {
        throw new Error('Wrong email or password.');
      }
    } catch (err) {
      this.res
        .status(HttpStatus.NOT_FOUND)
        .send({ errors: err });
    }
  }

  /**
   * Find by ID.
   *
   * @param {Number} id
   */
  async findById(id: Number) {
    try {
      const userProvider = new UserProvider();
      const result = await userProvider.findById(id);

      if (!result) {
        throw new Error('User does not exist.');
      }
      this.res.status(HttpStatus.OK).send(this.presenter.render(result));
    } catch (err) {
      console.log(err);
      this.res
        .status(HttpStatus.NOT_FOUND)
        .send({ errors: err });
    }
  }

  /**
   * Update single user by ID.
   *
   * @param {Number} id
   * @param {Object} body
   */
  async updateById(id: Number, body: Object) {
    try {
      const userProvider = new UserProvider();
      const store = new Store();
      const data = store.sync(body);
      if (!(data)) {
        return this.res
          .status(HttpStatus.BAD_REQUEST)
          .send({ errors: 'Illegal body data has been specified.' });
      }

      const saltRounds = 10;

      const password = data.password;

      if (password) {
        const bcryptProvider = new BcryptProvider();
        const hashedPassword = await bcryptProvider.bcryptEncrypt(password, saltRounds);

        data.password = hashedPassword;
      }

      const result = await userProvider.updateById(id, data);

      return this.res
        .status(HttpStatus.CREATED)
        .send(this.render(result));
    } catch (err) {
      console.log(err.message || err.title);
      return this.res
        .status(HttpStatus.BAD_REQUEST)
        .send({
          errors: 'Couldn\'t update user.'
        });
    }
  }
  
  /**
   * Create a new user.
   *
   * @param {Object} body
   */
  async create(body: any) {
    try {
      const userProvider = new UserProvider();
      const store = new Store();
      if (!(body.data && body.data.type === this.presenter.prototype.type)) {
        return this.res
          .status(HttpStatus.BAD_REQUEST)
          .send({ errors: `Data should contain the type "${this.presenter.prototype.type}".` });
      }
      const data = store.sync(body);
      if (!(data)) {
        return this.res
          .status(HttpStatus.BAD_REQUEST)
          .send({ errors: 'Illegal body data has been specified.' });
      }
      data.active = true;

      const saltRounds = 10;

      const password = data.password;
    
      const bcryptProvider = new BcryptProvider();
        const hashedPassword = await bcryptProvider.bcryptEncrypt(password, saltRounds);

      data.password = hashedPassword;

      // create the user
      const result = await userProvider.create(data);
      if (!result) {
        throw new Error('On create userProvider returns null.');
      }
      return this.res
        .status(HttpStatus.CREATED)
        .send(this.render(result));
    } catch (err) {
      console.log(err.message || err.title);
      console.log(err);
      return this.res
        .status(HttpStatus.BAD_REQUEST)
        .send({
          errors: err,
        });
    }
  }
}

module.exports = UserController;
