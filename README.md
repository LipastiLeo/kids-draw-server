# Kids-Draw-server

Kids-Draw simple server. Depends on infrastructure https://gitlab.com/LipastiLeo/kids-draw-infrastructure for environment variables. You should not set these .env variables yourself.

## DOCKER

If you are new to docker environment read more at https://docs.docker.com.<br />
Once you can run docker run hello-world without 'sudo' you are good to go.<br />

You have to use docker for dev in this project. Otherwise you have no database.

## TYPESCRIPT

This server uses typescript without compiling .js files by using ts-node.<br />
Note that all files must use typescript. js files will be ignored in commits.