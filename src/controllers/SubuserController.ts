// Export that fixes Typescript Cannot redeclare block-scoped variable.
export {};
const HttpStatus = require('http-status-codes');
const Controller = require('./Controller');
const SubuserModel = require('../models/sequelize/Subuser');
const { SubuserPresenter } = require('../presenters');
const SubuserProvider = require('../providers/SubuserProvider');

/**
 * @class SubuserController
 */
class SubuserController extends Controller {
  constructor(req: Object, res: Object) {
    super(req, res);
    this.presenter = SubuserPresenter;
    this.model = SubuserModel;
  }
  
  /**
   * List all created subusers
   */
  async list() {
    try {
      const subuserProvider = new SubuserProvider();
      const result = await subuserProvider.list();

      if (!result) {
        throw new Error('Nothing to list.');
      }
      this.res.status(HttpStatus.OK).send(result);
    } catch (err) {
      console.log(err);
      this.res
        .status(HttpStatus.NOT_FOUND)
        .send({ errors: err });
    }
  }

  /**
   * Create a new subuser.
   *
   * @param {Object} body
   */
  async create(body: Object) {
    try {
      // todo
    } catch (err) {
      console.log(err.message || err.title);
      return this.res
        .status(HttpStatus.BAD_REQUEST)
        .send({
          errors: err,
        });
    }
  }
}

module.exports = SubuserController;
