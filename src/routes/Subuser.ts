const express = require('express');
import { MiddlewareFn } from '../types/middleware';
const SubuserController = require('../controllers/SubuserController');
const authMiddleware = require('../auth/authMiddleware');

const router = express.Router();

// get subuser data - only user's own subusers
router.get('/', authMiddleware, <MiddlewareFn>function (req, res) {
  const controller = new SubuserController(req, res);
  return controller.list();
});

// update subuser data - only user's own subusers
router.patch('/:id/', authMiddleware, <MiddlewareFn>function (req, res) {
  const controller = new SubuserController(req, res);
  return controller.updateById(Number(req.params.id), req.body);
});

// create a new subuser under user account
router.post('/', authMiddleware, <MiddlewareFn>function (req, res) {
  const controller = new SubuserController(req, res);
  return controller.create(req.body);
});

module.exports = router;