FROM node:12

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY . /usr/src/app
RUN rm -rf node_modules
RUN npm install

EXPOSE 3000

CMD [ "npm", "start" ]