const express = require('express');
import { MiddlewareFn } from '../types/middleware';
const UserController = require('../controllers/UserController');
const authMiddleware = require('../auth/authMiddleware');

const router = express.Router();

// get user data - only user's own data
router.get('/:id/', authMiddleware, <MiddlewareFn>function (req, res) {
  const controller = new UserController(req, res);
  return controller.findById(Number(req.params.id));
});

// update user data - only user's own data
router.patch('/:id/', authMiddleware, <MiddlewareFn>function (req, res) {
  const controller = new UserController(req, res);
  return controller.updateById(Number(req.params.id), req.body);
});

// create a new user on register
router.post('/', <MiddlewareFn>function (req, res) {
  const controller = new UserController(req, res);
  return controller.create(req.body);
});

module.exports = router;