// Export that fixes Typescript Cannot redeclare block-scoped variable.
export {};
const { Sequelize, DataTypes } = require("sequelize");
const sql = require('../../sql');

/**
 * @typedef {Object} SubuserModel
 * @property {Number} id
 * @property {String} username
 * @property {String} password
 * @property {Boolean} isadmin
 * @property {Number} icon
 * @property {String} friendshipkey
 * @property {String} friendshippassword
 * @property {Number} userid
 * @property {Boolean} active
 * @property {String} reg_date
 */

/**
 * @type {SubuserModel}
 */
const Subuser = sql.define('subusers', {
  /**
   * id, primary key
   */
  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.BIGINT,
    field: 'id',
    validate: {
      isInt: true
    }
  },
  /**
   * username
   */
  username: {
    type: Sequelize.STRING(255),
    field: 'username',
    allowNull: false,
    validate: {
      notNull: {
        msg: 'Please give a username.'
      }
    }
  },
  /**
   * password - optional
   */
  password: {
    type: DataTypes.STRING(64),
    is: /^[0-9a-f]{64}$/i,
    field: 'password',
    allowNull: true
  },
  /**
   * isadmin - uses parental features
   */
  isadmin: {
    type: Sequelize.BOOLEAN,
    field: 'isadmin'
  },
  /**
   * icon - one of premade thumbnail user icons for the app
   */
  icon: {
    type: Sequelize.TINYINT,
    field: 'icon',
    validate: {
      isInt: true
    }
  },
  /**
   * friendshipkey
   */
  friendshipkey: {
    unique: true,
    type: Sequelize.STRING(255),
    field: 'friendshipkey'
  },
  /**
   * friendshippassword - string, not needed to securely store as hash password.
   * Can be null and only used to give a bit more control for friendship requests.
   */
  friendshippassword: {
    type: Sequelize.STRING(255),
    field: 'friendshippassword',
    allowNull: true
  },
  /**
   * userid
   */
  userid: {
    allowNull: false,
    foreignKey: true,
    type: Sequelize.BIGINT,
    field: 'userid',
    validate: {
      isInt: true
    }
  },
  /**
   * active - used to set false instead of delete
   */
  active: {
    type: Sequelize.BOOLEAN,
    field: 'active'
  },
  /**
   * update date
   */
  reg_date: {
    allowNull: true,
    type: Sequelize.DATE,
    field: 'reg_date',
    validate: {
      isDate: true
    }
  }
});

module.exports = Subuser;