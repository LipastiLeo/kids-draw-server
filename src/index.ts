// security
const helmet = require("helmet"); 
const AccessControl = require('express-ip-access-control');
const config = require('./config');
const user = require('./routes/User');
const subuser = require('./routes/Subuser');
//const friendship = require('./routes/Friendship');
//const drawing = require('./routes/Drawing');
const login = require('./routes/Login');
const token = require('./routes/Token');

// status codes
const HttpStatus = require('http-status-codes');

// express
const express = require('express');
const cookieParser = require('cookie-parser');
import {MiddlewareFn} from './types/middleware';
const bodyParser = require('body-parser');
const app = express();
app.use(cookieParser());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

// localhost for dev without docker
const localhostIP = '::ffff:127.0.0.1';
// Whitelist for docker container || localhost
const ips = [process.env.REACT_IP || localhostIP]

app.use(AccessControl({
  // if not in docker environment we change to deny so local dev environment works.
  mode: process.env.REACT_IP ? 'allow' : 'deny',
  denys: [],
  allows: [ips],
  forceConnectionAddress: false,
  statusCode: 401,
  redirectTo: '',
  message: 'Unauthorized'
}));

app.set('port', config.PORT);

app.use(helmet());

// Implement health/status call
app.use('/health', <MiddlewareFn>function (req, res, next) {
  let reqid = 0;
    res.header('Content-Type', 'text/plain; charset=utf-8');
    res.status(HttpStatus.OK).send();
});

// /users
app.use('/users', user);

// /subusers
app.use('/subusers', subuser);

// /friendships
//app.use('/friendships', friendship);

// /drawings
//app.use('/drawings', drawing);

// /refresh token
app.use('/token', token);

// /login
app.use('/login', login);

// Express error logging
app.on('error', <MiddlewareFn>function (err) {
  console.log(`Express: ${err}`);
});

app.use('*', <MiddlewareFn>function (req, res, next) {
  console.log(req.url);
});

// Start web server using defined port
app.listen(app.get('port'), () => {
  console.log(`Server is running on port ${app.get('port')}`);
});
