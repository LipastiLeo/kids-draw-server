# auth Middleware

authMiddleware is used to verify tokens.<br />
Wrong token results 403.<br />
Expired token results 409.<br />
no token results to 401