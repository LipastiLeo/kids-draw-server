const Sequelize = require('sequelize');

const sequelize = new Sequelize(
  'kidsdraw',
  process.env.DB_USER,
  process.env.DB_PASSWORD,
  {
    host: process.env.DB_IP,
    dialect: 'mysql',
    logging: false,
    define: {
      timestamps: false
    }
  }
);

module.exports = sequelize;
