const UserModel = require('./sequelize/User');
const SubuserModel = require('./sequelize/Subuser');

module.exports = {
  UserModel,
  SubuserModel,
};
