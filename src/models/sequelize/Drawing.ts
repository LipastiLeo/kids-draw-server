// Export that fixes Typescript Cannot redeclare block-scoped variable.
export {};
const { Sequelize, DataTypes } = require("sequelize");
const sql = require('../../sql');

/**
 * @typedef {Object} DrawingModel
 * @property {Number} id
 * @property {Number} authorid
 * @property {String} uniqcode
 * @property {String} link
 * @property {String} reg_date
 */

/**
 * @type {DrawingModel}
 */
const Drawing = sql.define('drawings', {
  /**
   * id, primary key
   */
  id: {
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.BIGINT,
    field: 'id',
    validate: {
      isInt: true
    }
  },
  /**
  * authorid
  */
  authorid: {
    allowNull: false,
    foreignKey: true,
    type: Sequelize.BIGINT,
    field: 'authorid',
    validate: {
      isInt: true
    }
  },
  /**
   * uniqcode
   */
  uniqcode: {
    foreignKey: true,
    type: Sequelize.STRING(255),
    field: 'uniqcode'
  },
  /**
   * link
   */
  link: {
    type: Sequelize.STRING(255),
    field: 'link'
  },
  /**
   * update date
   */
  reg_date: {
    allowNull: true,
    type: Sequelize.DATE,
    field: 'reg_date',
    validate: {
      isDate: true
    }
  }
});

module.exports = Drawing;