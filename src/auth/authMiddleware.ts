// Export that fixes Typescript Cannot redeclare block-scoped variable.
export {};
import { MiddlewareFn } from '../types/middleware';
import { User } from '../types/jwt';
import { Error } from '../types/jwterror';
const jwt = require('jsonwebtoken');

const accessTokenSecret = process.env.accessTokenSecret || 'localdevTokens';

const authMiddleware = <MiddlewareFn>function (req, res, next) {
  const authHeader = req.headers.authorization;

  if (authHeader) {
      const token = authHeader.split(' ')[1];

      jwt.verify(token, accessTokenSecret, (err: Error, decoded: User) => {
          if (!!err && err.name === 'TokenExpiredError') {
              // 409 will trigger client to use refreshtoken
              return res.sendStatus(409);
          } else if (err) {
            return res.sendStatus(403);
          }
          // we force req.params.id to be userid in this app
          req.params.id = decoded.id.toString();
          next();
      });
  } else {
      res.sendStatus(401);
  }
};

module.exports = authMiddleware;